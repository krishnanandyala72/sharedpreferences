package hari.sharedpreferences;

import android.app.Application;

import com.facebook.stetho.Stetho;


public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
//        chrome://inspect/#devices
//        http://facebook.github.io/stetho/
        //Stetho..
        Stetho.initializeWithDefaults(this);
    }
}
