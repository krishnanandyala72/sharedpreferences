package hari.sharedpreferences;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StoreActivity extends AppCompatActivity {
    private Button save_data, next_activity;
    private EditText your_text;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //pull views
        your_text = (EditText) findViewById(R.id.your_text);
        save_data = (Button) findViewById(R.id.save_data);
        next_activity = (Button) findViewById(R.id.next_activity);

        //set Listeners
        save_data.setOnClickListener(new SaveData());
        next_activity.setOnClickListener(nextActivityClickListen);

        //create shared preferences
        sharedPreferences = getSharedPreferences("MyData", MODE_PRIVATE);

        //create SP editor
        editor = sharedPreferences.edit();

    }

    View.OnClickListener nextActivityClickListen = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(StoreActivity.this, RetrieveActivity.class);
            startActivity(intent);
        }
    };

    private class SaveData implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (your_text.getText().toString().length() == 0) {
                your_text.setError("Should not be empty.");
                return;
            }
            //put data in SP
            editor.putString("data", your_text.getText().toString());
            editor.apply();
            Toast.makeText(StoreActivity.this, "Your data saved.", Toast.LENGTH_LONG).show();
        }
    }

}
